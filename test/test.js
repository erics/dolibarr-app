#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

const { SSL_OP_EPHEMERAL_RSA } = require('constants');

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    util = require('util'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var TEST_TIMEOUT = 50000;
    var app;
    var apiEndpoint;

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function getAppInfo(callback) {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        apiEndpoint = inspect.apiEndpoint;

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');

        callback();
    }

    function welcomePage(callback) {
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(by.xpath('//*[contains(text(), "Dolibarr")]'));
        }).then(function () {
            callback();
        });
    }

    function checkDash(callback) {
        browser.get('https://' + app.fqdn + '/').then(function () {
            return waitForElement(by.xpath('//*[@id="topmenu-login-dropdown"]/a/span[contains(text(), "admin")]'));
        }).then(function () {
            callback();
        });
    }

    function visible(selector) {
        return browser.wait(until.elementLocated(selector), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
        });
    }

    function login(user, pass, callback) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn).then(function () {
            return visible(by.id('username'));
        }).then(function () {
            return browser.findElement(by.id('username')).sendKeys(user);
        }).then(function () {
            return browser.findElement(by.id('password')).sendKeys(pass);
        }).then(function () {
            // return browser.findElement(by.className('lw-btn')).click();
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('id-left')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }


    function checkCron(callback) {
        this.timeout(60000 * 2);

        fs.writeFileSync('/tmp/crontab', '* * * * * echo -n "$CLOUDRON_MYSQL_HOST" > /app/data/public/cron\n', 'utf8');
        execSync(`cloudron push --app ${app.id} /tmp/crontab /app/data/crontab`);
        fs.unlinkSync('/tmp/crontab');

        execSync(`cloudron restart --app ${app.id}`);

        console.log('Waiting for crontab to trigger');

        setTimeout(function () {
            superagent.get('https://' + app.fqdn + '/cron').end(function (error, result) {
                if (error && !error.response) return callback(error); // network error

                if (result.statusCode !== 200) return callback('Expecting 200, got ' + result.statusCode);

                if (result.text !== 'mysql') return callback('Unexpected text: ' + result.text);

                callback();
            });
        }, 60 * 1000); // give it a minute to run the crontab
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    describe('installation and configuration', function () {
        it('install app', function () {
            execSync(`cloudron install --location ${LOCATION}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        });

        it('can get app information', getAppInfo);
        it('can view welcome page', welcomePage);
        it('can login', login.bind(null, 'admin', 'admin123'));
        it('can access dashboard', checkDash);

        // it('executes cron tasks', checkCron);

        it('backup app', function () {
            execSync(`cloudron backup create --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        });

        it('restore app', function () {
            const backups = JSON.parse(execSync('cloudron backup list --raw'));
            execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
            execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        });

        it('move to different location', function () {
            browser.manage().deleteAllCookies();
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');
        });

        it('can login', login.bind(null, 'admin', 'admin123'));
        it('can access dashboard', checkDash);

        it('uninstall app', function () {
            execSync(`cloudron uninstall --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        });
    });

    describe('update', function () {
        // test update
        it('can install app', function () {
            execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
            expect(app).to.be.an('object');
        });

        it('can get app information', getAppInfo);
        it('can view welcome page', welcomePage);
        // it('can access dashboard', checkDash);

        it('uninstall app', function () {
            execSync(`cloudron uninstall --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        });
    });
});
