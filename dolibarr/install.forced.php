<?php
/* Copyright (C) 2016 Raphaël Doursenaud <rdoursenaud@gpcsolutions.fr>
 * Copyright (C) 2020 Eric Seigne <eric.seigne@informatique-libre.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/** @var bool Hide PHP informations */
$force_install_nophpinfo = true;
/** @var int 1 = Lock and hide environment variables, 2 = Lock all set variables */
$force_install_noedit = 2;
$force_install_lockinstall = true;

$force_install_message = 'Welcome to your Cloudron install of Dolibarr';
$force_install_main_data_root = '/app/data/dolibarr';
$force_install_mainforcehttps = false;

// Install Database
$force_install_type = 'mysqli';
$force_install_dbserver = getenv('CLOUDRON_MYSQL_HOST');
$force_install_port = getenv('CLOUDRON_MYSQL_PORT');
$force_install_prefix = 'dolibarr_';
$force_install_createdatabase = false;
$force_install_database = getenv('CLOUDRON_MYSQL_DATABASE');
$force_install_databaselogin = getenv('CLOUDRON_MYSQL_USERNAME');
$force_install_databasepass = getenv('CLOUDRON_MYSQL_PASSWORD');
$force_install_createuser = false;
//$force_install_databaserootlogin = 'root';
//$force_install_databaserootpass = '';

/** @var bool Force install locking */
$force_install_lockinstall = false;

/** @var string Dolibarr super-administrator username */
$force_install_dolibarrlogin = 'admin';

/** @var string Enable module(s) (Comma separated class names list) */
$force_install_module = 'modLdap,modSociete,modFournisseur,modFacture';

