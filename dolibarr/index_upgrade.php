<?php
http_response_code(425);
header("Refresh:20");
?>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://doliscan.fr/css/app.css" rel="stylesheet">
    <title>Installation en cours</title>
</head>

<body class="d-flex flex-column h-100">
    <main role="main" class="flex-shrink-0 py-3">
        <div class="container">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Installation en cours ...</div>

                            <div class="card-body">
                                <p>Merci de bien vouloir patienter, la création de la base de données est en cours ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">L'application</h5>
                </div>
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Partenaires</h5>
                    <ul>
                        <li><a href="https://osinum.fr">Osinum</a></li>
                        <li><a href="https://informatique-libre.com">Informatique-Libre</a></li>
                        <li><a href="https://cap-rel.fr">CAP-REL</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Divers</h5>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
