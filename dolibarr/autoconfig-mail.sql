-- auto configuration for mail system

REPLACE INTO dolibarr_const (`name`, `value`, `type`) VALUES
('MAIN_MAIL_EMAIL_STARTTLS', '0', 'chaine'),
('MAIN_MAIL_EMAIL_TLS', '0', 'chaine'),
('MAIN_MAIL_SENDMODE', 'smtps', 'chaine'),
('MAIN_MAIL_DEFAULT_FROMTYPE', 'user', 'chaine'),
('MAIN_MAIL_SMTP_SERVER', 'CLOUDRON_MAIL_SMTP_SERVER', 'chaine'),
('MAIN_MAIL_EMAIL_FROM', 'CLOUDRON_MAIL_FROM', 'chaine'),
('MAIN_MAIL_SMTPS_ID', 'CLOUDRON_MAIL_SMTP_USERNAME', 'chaine'),
('MAIN_MAIL_SMTPS_PW', 'CLOUDRON_MAIL_SMTP_PASSWORD', 'chaine'),
('MAIN_MAIL_SMTP_PORT','CLOUDRON_MAIL_SMTP_PORT','chaine');

-- others options

-- MAIN_MAIL_AUTOCOPY_TO 	contact@cap-rel.fr 	1
-- MAIN_MAIL_DEFAULT_FROMTYPE 	user 	1
-- MAIN_MAIL_ENABLED_USER_DEST_SELECT 	0 	1
-- MAIN_MAIL_ERRORS_TO 	robot@cap-rel.fr 	1
-- MAIN_MAIL_USE_MULTI_PART 	1
