#!/bin/bash
set -eu

FIRST_TIME=0

DOLVERSION=13.0.1

mkdir -p /run/dolibarr/sessions /app/data/dolibarr /app/data/conf /run/php/sessions

# just in case ...
chown www-data:www-data /run/php/sessions

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [ ! -f /app/data/dolibarr/.dbsetup ]; then
    FIRST_TIME=1
    echo "Fresh installation, performing Dolibarr first time setup"
    #change dolibarr homepage during database install
    rm -f /app/data/index.php
    ln -s /app/data/index_upgrade.php /app/data/index.php
    
    # Install with CURL CLI call, thanks to YunoHost-Apps/dolibarr
    APACHE_CONFDIR="" source /etc/apache2/envvars
    chown -R www-data:www-data /app/data /run/dolibarr
    /usr/sbin/apache2 -DBACKGROUND
    
    LOCALURI="http://localhost:8000/install"
    echo " - STEP N°1 ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/fileconf.php > /dev/null 2>&1
    echo "   [done]"
    echo " - STEP N°2 ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step1.php --data "testpost=ok&action=set&selectlang=fr_FR" > /dev/null 2>&1
    echo "   [done]"
    echo " - STEP N°3: create database (could take a lot of time) ... "
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step2.php --data "testpost=ok&action=set&dolibarr_main_db_character_set=latin1&dolibarr_main_db_collation=latin1_swedish_ci&selectlang=fr_FR" > /dev/null 2>&1
    echo "   [done]"
    echo " - STEP N°4 ..."
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step4.php --data "testpost=ok&action=set&selectlang=fr_FR" > /dev/null 2>&1
    echo "   [done]"
    echo " - STEP N°5 ... creating account (default is admin / admin123) ..."
    curl -kL -H "Host: localhost" -X POST ${LOCALURI}/step5.php --data "testpost=ok&action=set&selectlang=fr_FR&pass=admin123&pass_verif=admin123" > /dev/null 2>&1
    echo "   [done]"
    
    #Use LDAP as auth source
    if [ -f /app/data/conf/conf.php.ldap -a -f /app/data/conf/conf.php ]; then
        cat /app/data/conf/conf.php.ldap >> /app/data/conf/conf.php
    else
        echo "ERROR, /app/data/conf/conf.php or /app/data/conf/conf.php.ldap does not exists !"
    fi
    
    # Configuration for langs
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /app/data/conf/autoconfig-langs.sql
    
    # Use cloudron DB setup
    sed -i -e s/".*dolibarr_main_db_port.*"/"\$dolibarr_main_db_port=getenv('CLOUDRON_MYSQL_PORT');"/ /app/data/conf/conf.php
    sed -i -e s/".*dolibarr_main_db_name.*"/"\$dolibarr_main_db_name=getenv('CLOUDRON_MYSQL_DATABASE');"/ /app/data/conf/conf.php
    sed -i -e s/".*dolibarr_main_db_user.*"/"\$dolibarr_main_db_user=getenv('CLOUDRON_MYSQL_USERNAME');"/ /app/data/conf/conf.php
    sed -i -e s/".*dolibarr_main_db_pass.*"/"\$dolibarr_main_db_pass=getenv('CLOUDRON_MYSQL_PASSWORD');"/ /app/data/conf/conf.php
    
    #install.lock is not usable due to cloudron exclude for backups
    touch /app/data/dolibarr/.dbsetup
    echo "Done."
    
    if [ -f "${APACHE_PID_FILE}" ]; then
        LEPID=`cat ${APACHE_PID_FILE}`
        kill ${LEPID} || true
    fi
    
    #force first LDAP sync (could be unsuccessfull)
    TESTCONF=`grep "dolibarr_main_auth_ldap_port" /app/data/conf/conf.php`
    if [ -z "${TESTCONF}" ]; then
        echo "conf.php does not contain LDAP configuration part, we will try to add it now"
        cat /app/data/conf/conf.php.ldap >> /app/data/conf/conf.php
    fi
    TESTCONF=`grep "dolibarr_main_auth_ldap_port" /app/data/conf/conf.php`
    if [ -z "${TESTCONF}" ]; then
        echo "ERROR: ldap configuration is not ready !"
    fi
    
    rm -f /app/data/index.php
    ln -s /app/data/index_prod.php /app/data/index.php
else
    #upgrade
    rm -f /app/data/dolibarr/install.lock
    
    #remove old upgrade logs
    rm -f /var/data/dolibarr/upgrade-*.html
    
    #starting upgrade
    cd /app/code/htdocs/install
    DOLPREVVERSION=`php /app/code/htdocs/install/get-version.php`
    if php /app/code/htdocs/install/upgrade.php ${DOLPREVVERSION} ${DOLVERSION} > /app/data/dolibarr/upgrade-01.html; then
        echo " - Upgrade step N°1 ... done"
    else
        echo " - Upgrade step N°1 error"
    fi
    
    if php /app/code/htdocs/install/upgrade2.php ${DOLPREVVERSION} ${DOLVERSION} > /app/data/dolibarr/upgrade-02.html; then
        echo " - Upgrade step N°2 ... done"
    else
        echo " - Upgrade step N°2 error"
    fi
    
    if php /app/code/htdocs/install/step5.php ${DOLPREVVERSION} ${DOLVERSION} > /app/data/dolibarr/upgrade-03.html; then
        echo " - Upgrade step N°3 ... done"
    else
        echo " - Upgrade step N°3 error"
    fi
    
fi

# Auto config LDAP (can change in case of backup / restaure on other server)
rm -f /app/data/ldap/autoconfig-ldap.sql
cat /app/data/ldap/autoconfig-ldap.sql.default  | sed s/"CLOUDRON_LDAP_SERVER"/${CLOUDRON_LDAP_SERVER}/ \
| sed s/"CLOUDRON_LDAP_PORT"/${CLOUDRON_LDAP_PORT}/ \
| sed s/"CLOUDRON_LDAP_USERS_BASE_DN"/${CLOUDRON_LDAP_USERS_BASE_DN}/ \
> /app/data/ldap/autoconfig-ldap.sql
if [ -f /app/data/ldap/autoconfig-ldap.sql ]; then
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /app/data/ldap/autoconfig-ldap.sql
fi

if [ "${FIRST_TIME}" -eq 1 ]; then
    /usr/bin/php /app/code/scripts/user/sync_users_ldap2dolibarr.php -y || true
fi

# Auto config MAIL (can change in case of backup / restaure on other server)
rm -f /app/data/conf/autoconfig-mail.sql
cat /app/data/conf/autoconfig-mail.sql.default  | sed s/"CLOUDRON_MAIL_SMTP_SERVER"/${CLOUDRON_MAIL_SMTP_SERVER}/ \
| sed s/"CLOUDRON_MAIL_FROM"/${CLOUDRON_MAIL_FROM}/ \
| sed s/"CLOUDRON_MAIL_SMTP_USERNAME"/${CLOUDRON_MAIL_SMTP_USERNAME}/ \
| sed s/"CLOUDRON_MAIL_SMTP_PASSWORD"/${CLOUDRON_MAIL_SMTP_PASSWORD}/ \
| sed s/"CLOUDRON_MAIL_SMTP_PORT"/${CLOUDRON_MAIL_SMTP_PORT}/ \
> /app/data/conf/autoconfig-mail.sql
if [ -f /app/data/conf/autoconfig-mail.sql ]; then
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /app/data/conf/autoconfig-mail.sql
fi

#for dolibarr, but .lock files seems to be excluded from backups (?)
touch /app/data/dolibarr/install.lock

chown -R www-data:www-data /app/data /run/dolibarr

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
