FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code /app/data/dolibarr /app/pkg
WORKDIR /app/code

# required packages
RUN apt-get -y update && \
    apt-get install -y cron geoip-database php7.4 php7.4-{apcu,apcu-bc,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,geoip,gnupg,gmp,imap,imagick,interbase,intl,json,ldap,mbstring,mysql,odbc,opcache,pspell,readline,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php7.4 && \
    apt-get install -y php-{date,gettext,pear,twig,validate} && \
    apt-get remove -y php7.3* libapache2-mod-php7.3 && apt-get autoremove -y && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/dolibarr.conf /etc/apache2/sites-enabled/dolibarr.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod rewrite headers php7.4 expires deflate mime dir rewrite setenvif

# configure mod_php
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/dolibarr/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

RUN rm -rf /var/lib/php/sessions && ln -s /run/php/sessions /var/lib/php/sessions

RUN cp /etc/php/7.4/apache2/php.ini /etc/php/7.4/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/7.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/7.4/cli/conf.d/99-cloudron.ini

# download dolibarr
ARG VERSION=13.0.1
RUN curl -L https://github.com/Dolibarr/dolibarr/archive/${VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code

# config file
RUN mkdir -p /app/data/conf
RUN rm -f /app/code/htdocs/conf/conf.php
RUN touch /app/data/conf/conf.php
RUN ln -s /app/data/conf/conf.php /app/code/htdocs/conf/conf.php
COPY dolibarr/conf.php.ldap /app/data/conf/conf.php.ldap

# more things for ldap setup
RUN mkdir -p /app/data/ldap
COPY dolibarr/autoconfig-ldap.sql /app/data/ldap/autoconfig-ldap.sql.default
COPY dolibarr/autoconfig-langs.sql /app/data/conf/autoconfig-langs.sql
COPY dolibarr/autoconfig-mail.sql /app/data/conf/autoconfig-mail.sql.default

COPY dolibarr/install.forced.php /app/code/htdocs/install/install.forced.php
COPY start.sh /app/pkg/

# custom folder is for new plugins, we have to be able to deploy new plugins
RUN mv /app/code/htdocs/custom /app/data/custom
RUN ln -s /app/data/custom /app/code/htdocs/custom

# special index.php "please wait while upgrade is in progress" hack
RUN mv /app/code/htdocs/index.php /app/data/index_prod.php
RUN touch /app/data/index.php
RUN ln -s /app/data/index.php /app/code/htdocs/index.php
COPY dolibarr/index_upgrade.php /app/data/index_upgrade.php

# special cron file without secure key
RUN mkdir -p /app/data/scripts
COPY dolibarr/cron_run_jobs_custom.php /app/data/scripts/cron_run_jobs_custom.php

# get dolibarr version
COPY dolibarr/get-version.php /app/code/htdocs/install/get-version.php

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

CMD [ "/app/pkg/start.sh" ]
