This app packages Dolibarr <upstream>13.0.1</upstream>

## About

There are several feature modules that can be enabled or disabled, as needed. This software is free under GNU General Public License 3.0. It is a web-based application, and can therefore be used wherever an internet service is available. Dolibarr aims to offer free open source ERP and CRM features for people with no technical knowledge, by providing a simple solution.

Dolibarr includes all the important features of an ERP CRM suite. It is modular and is thus characterized by its ease of installation and use, despite the large number of features.


## Features

Main modules:
 * Sales Management
 * Purchase Management
 * Customer Relationship Management
 * Products-and-services catalog
 * Stock Management
 * Calendar
 * Event Management
 * Bank account management
 * Address book
 * Foundation-members management
 * Payments management
 * Donations management
 * Commercial actions management
 * Commercial proposals management
 * Contracts management
 * Orders management
 * Standing orders management
 * Shipping management
 * Point of sale
 * Electronic document management
 * Project Management
 * Surveys
 * PDF and OpenDocument generation
 * Reporting
 * Wizard to help to export/import data
 * LDAP connectivity

Miscellaneous:
 * Multi-user, with several permissions levels for each feature.
 * Multi-language
 * Multi-currency
 * User-friendly
 * Assorted skins
 * Code is highly customizable (modular).
 * Works with MySQL 4.1 or higher, or PostgreSQL
 * Works with PHP

