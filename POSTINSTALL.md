This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: admin123<br/>
**Email**: admin@cloudron.local<br/>

Please change the admin password and email immediately.

All Cloudron LDAP users can use Dolibarr but you have to use admin account to delegate rights to your users.
